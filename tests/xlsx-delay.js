var ss = require('../index'),
    xlutil = ss.util,
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    util = require('util'),
    delay = require('./testdata/definitions/delayTest');

logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});

module.exports = exports = {
    setUp: function(cb) {
        cb();
    },
    tearDown: function(cb) {
        cb();
    },
    import: {
        happy: {
            setUp: function(cb) {
                logger.silly('import setup!');
                this.delay = new delay();
                this.import = new ss.import();
                cb();
            },
            one: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/delay/happy/one.xlsx'));
                self.import.process(this.delay.sheet, 'hello', './testdata/xlsx/delay/happy/one.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.delayTest);
                    test.ok( !! self.import.importData.delayTest.data);
                    logger.silly(util.inspect(self.import.importData.delayTest));
                    test.equal(self.import.importData.delayTest.data.length, 1, "record processing problem.");
                    test.done();
                });
            },
            none: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/delay/happy/none.xlsx'));
                self.import.process(this.delay.sheet, 'hello', './testdata/xlsx/delay/happy/none.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.delayTest);
                    test.ok( !! self.import.importData.delayTest.data);
                    logger.silly(util.inspect(self.import.importData.delayTest));
                    test.equal(self.import.importData.delayTest.data.length, 0, "record processing problem.");
                    test.done();
                });
            },
            many: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/delay/happy/many.xlsx'));
                self.import.process(this.delay.sheet, 'hello', './testdata/xlsx/delay/happy/many.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.delayTest);
                    test.ok( !! self.import.importData.delayTest.data);
                    logger.silly(util.inspect(self.import.importData.delayTest))
                    test.equal(self.import.importData.delayTest.data.length, 10, "record processing problem.");
                    test.done();
                });
            }
        },
        cellDataProblem: {
            setUp: function(cb) {
                this.delay = new delay();
                this.import = new ss.import();
                cb();
            },
            one: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/delay/cellDataProblem/one.xlsx'));
                self.import.process(this.delay.sheet, 'hello', './testdata/xlsx/delay/cellDataProblem/one.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.delayTest);
                    test.ok( !! self.import.importData.delayTest.data);
                    logger.silly(util.inspect(self.import.importData.delayTest));
                    test.equal(self.import.importData.delayTest.invalidData.length, 1, "invalid record recognized as valid");
                    test.equal(self.import.importData.delayTest.data.length, 9, "more than one valid record recognized as invalid");
                    test.done();
                });
            },
            all: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/delay/cellDataProblem/many.xlsx'));
                self.import.process(this.delay.sheet, 'hello', './testdata/xlsx/delay/cellDataProblem/many.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.delayTest);
                    test.ok( !! self.import.importData.delayTest.data);
                    logger.silly(util.inspect(self.import.importData.delayTest));
                    test.equal(self.import.importData.delayTest.invalidData.length, 10, "invalid record recognized as valid");
                    test.equal(self.import.importData.delayTest.data.length, 0, "more than one valid record recognized as invalid");
                    test.done();
                });
            }
        },
        rowDataProblem: {
            setUp: function(cb) {
                this.delay = new delay();
                this.import = new ss.import();
                cb();
            },
            one: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/delay/rowDataProblem/one.xlsx'));
                self.import.process(this.delay.sheet, 'hello', './testdata/xlsx/delay/rowDataProblem/one.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.delayTest);
                    test.ok( !! self.import.importData.delayTest.data);
                    logger.silly(util.inspect(self.import.importData.delayTest));
                    test.equal(self.import.importData.delayTest.invalidData.length, 1, "invalid record recognized as valid");
                    test.equal(self.import.importData.delayTest.data.length, 9, "more than one valid record recognized as invalid");
                    test.done();
                });
            },
            all: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/delay/rowDataProblem/many.xlsx'));
                self.import.process(this.delay.sheet, 'hello', './testdata/xlsx/delay/rowDataProblem/many.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.delayTest);
                    test.ok( !! self.import.importData.delayTest.data);
                    logger.silly(util.inspect(self.import.importData.delayTest));
                    test.equal(self.import.importData.delayTest.invalidData.length, 10, "invalid record recognized as valid");
                    test.equal(self.import.importData.delayTest.data.length, 0, "more than one valid record recognized as invalid");
                    test.done();
                });
            }
        }

    }
};
