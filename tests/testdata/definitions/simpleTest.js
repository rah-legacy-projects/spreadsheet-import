var logger = require('winston'),
    util = require('util'),
    _ = require('lodash');

var SimpleTestImport = function() {
    var self = this;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'Simple Test Import',
        className: 'simpleTest',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Foo',
            propertyName: 'foo',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Foo needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\d+/,
                errorMessage: '[rules needed for Foo]'
            }]
        }, {
            name: 'Bar Baz',
            propertyName: 'barBaz',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Bar Baz needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w+/,
                errorMessage: '[rules needed for Bar Baz]'
            }]
        }, {
            name: 'Identifier',
            propertyName: 'identifier',
            expectedType: 'string',
            isLocked: false,
            isIdentity: true,
            columnHelp: [{
                text: '[column help for Identifier needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /\w+/,
                errorMessage: '[rules needed for Identifier]'
            }]
        }, {
            name: 'Date',
            propertyName: 'date',
            expectedType: 'date',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Date needed]',
                ordinal: 0
            }],
            validators: [{
                validator: /^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))?$/,
                errorMessage: '[rules needed for Date]'
            }]
        }]
    };

    self.diff = function(row, index, cb) {
        var loadedthing = _.clone(row);
        if (index > 2) {
            loadedthing.identifier = 'eff dee ess ey';
        }
        cb(null, loadedthing);
    };

    self.commit = function() {
        logger.silly('todo: commit');
    };
    self.exportData = function(parameters, callback) {
        logger.silly('export parameters: ' + util.inspect(parameters));
        callback(null, [{
            foo: '5',
            barBaz: 'hello',
            identifier: 'there',
            date: '5/5/5555'
        }]);
    };
    self.sheet.rowValidators = [{
        validator: function(value, callback) {
            logger.silly('validator for simpleTest needed');
            if (/[zqkfjpvw]/.test(value.barBaz)) {
                callback(null, false);
            } else {
                callback(null, true);
            }
        },
        errorMessage: '[error messages for simpleTest needed]'
    }];
    return self;
};
SimpleTestImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = SimpleTestImport;
