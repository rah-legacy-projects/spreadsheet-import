var logger = require('winston');

var delayTestImport = function() {
    var self = this;
    self.sheet = {
        isIgnored: false,
        isProtected: false,
        sheetName: 'delay Test Import',
        className: 'delayTest',
        sheetHelp: [{
            text: '[sheet help copy needed]',
            ordinal: 0
        }],
        columns: [{
            name: 'Foo',
            propertyName: 'foo',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Foo needed]',
                ordinal: 0
            }],
            validators: [{
                validator: function(value, callback) {
                    setTimeout(function() {
                        callback(null, /\d+/.test(value));
                    }, 1000);
                },
                errorMessage: '[rules needed for Foo]'
            }]
        }, {
            name: 'Bar Baz',
            propertyName: 'barBaz',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Bar Baz needed]',
                ordinal: 0
            }],
            validators: [{
                validator: function(value, callback) {
                    setTimeout(function() {
                        callback(null, /\w+/.test(value));
                    }, 1000);
                },
                errorMessage: '[rules needed for Bar Baz]'
            }]
        }, {
            name: 'Identifier',
            propertyName: 'identifier',
            expectedType: 'string',
            isLocked: false,
            columnHelp: [{
                text: '[column help for Identifier needed]',
                ordinal: 0
            }],
            validators: [{
                validator: function(value, callback) {
                    setTimeout(function() {
                        callback(null, /\w+/.test(value));
                    }, 1000);
                },
                errorMessage: '[rules needed for Identifier]'
            }]
        }]
    };
    self.commit = function() {
        logger.silly('todo: commit');
    };
    self.sheet.rowValidators = [{
        validator: function(value, callback) {
            setTimeout(function() {
                logger.silly('validator for delayTest needed');
                if (/[zqkfjpvw]/.test(value.barBaz)) {
                    callback(null, false);
                } else {
                    callback(null, true);
                }
            }, 2000);
        },
        errorMessage: '[error messages for delayTest needed]'
    }];
    return self;
};
delayTestImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);
module.exports = delayTestImport;
