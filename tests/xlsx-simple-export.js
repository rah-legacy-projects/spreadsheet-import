var ss = require('../index'),
    xlutil = ss.util,
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    util = require('util');

var simple = require('./testdata/definitions/simpleTest');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});

module.exports = exports = {
    setUp: function(cb) {
        cb();
    },
    tearDown: function(cb) {
        cb();
    },
    export: {
        happy: {
            setUp: function(cb) {

                logger.silly('import setup!');
                this.simple = new simple();
                this.export = new ss.export();
                cb();
            },
            one: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/simple/happy/one.xlsx'));
                self.export.process(this.simple, null, function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.export.exportData);
                    test.ok( !! self.export.exportData.simpleTest);
                    test.ok( !! self.export.exportData.simpleTest.data);
                    logger.silly(util.inspect(self.export.exportData.simpleTest));
                    test.equal(self.export.exportData.simpleTest.data.length, 1, "record processing problem.");
                    test.done();
                });
            },
        },
        'cell negative': {
            setUp: function(cb) {
                this.simple = new simple();
                this.simple.exportData = function(parameters, callback) {
                    logger.silly('overriden export parameters: ' + util.inspect(parameters));
                    callback(null, [{
                        foo: 'fff',
                        barBaz: 'hello',
                        identifier: 'there',
                        date: '5/5/5555'
                    }]);
                };
                this.export = new ss.export();
                cb();
            },
            one: function(test) {
                var self = this;
                self.export.process(this.simple, null, function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.export.exportData);
                    test.ok( !! self.export.exportData.simpleTest);
                    test.ok( !! self.export.exportData.simpleTest.invalidData);
                    logger.silly(util.inspect(self.export.exportData.simpleTest));
                    test.equal(self.export.exportData.simpleTest.invalidData.length, 1, "record processing problem.");
                    test.done();
                });

            }
        },
        'row negative': {
            setUp: function(cb) {
                this.simple = new simple();
                this.simple.exportData = function(parameters, callback) {
                    logger.silly('overriden export parameters: ' + util.inspect(parameters));
                    callback(null, [{
                        foo: '5',
                        //this should cause row validation to fail
                        barBaz: 'hellozzz',
                        identifier: 'there',
                        date: '5/5/5555'
                    }]);
                };
                this.export = new ss.export();
                cb();
            },
            one: function(test) {
                var self = this;
                self.export.process(this.simple, null, function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.export.exportData);
                    test.ok( !! self.export.exportData.simpleTest);
                    test.ok( !! self.export.exportData.simpleTest.invalidData);
                    logger.silly(util.inspect(self.export.exportData.simpleTest));
                    test.equal(self.export.exportData.simpleTest.invalidData.length, 1, "record processing problem.");
                    test.done();
                });
            }
        }
    }
};
