var ss = require('../index'),
    xlutil = ss.util,
    fs = require('fs'),
    path = require('path');
module.exports = exports = {
    setUp:function(cb){
        cb();
    },
    tearDown: function(cb){
        cb();
    },
    imports: function(test){
       test.ok(fs.existsSync('./tests/testdata/simple-csv.csv'));
       test.done();
    },
    exports: function(test){
        test.done();
    }
};
