var ss = require('../index'),
    xlutil = ss.util,
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    util = require('util');

var simple = require('./testdata/definitions/simpleTest');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});
process.on('uncaughtException', function(err){
    logger.error(err);
    logger.error(err.stack);
});
module.exports = exports = {
    setUp: function(cb) {
        cb();
    },
    tearDown: function(cb) {
        cb();
    },
    import: {
        happy: {
            setUp: function(cb) {

                logger.silly('import setup!');
                this.simple = new simple();
                this.import = new ss.import();
                cb();
            },
            one: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/simple/happy/one.xlsx'));
                self.import.process(this.simple.sheet, 'hello', './testdata/xlsx/simple/happy/one.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.simpleTest);
                    test.ok( !! self.import.importData.simpleTest.data);
                    logger.silly(util.inspect(self.import.importData.simpleTest));
                    test.equal(self.import.importData.simpleTest.data.length, 1, "record processing problem.");
                    test.done();
                });
            },
            none: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/simple/happy/none.xlsx'));
                self.import.process(this.simple.sheet, 'hello', './testdata/xlsx/simple/happy/none.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.simpleTest);
                    test.ok( !! self.import.importData.simpleTest.data);
                    logger.silly(util.inspect(self.import.importData.simpleTest));
                    test.equal(self.import.importData.simpleTest.data.length, 0, "record processing problem.");
                    test.done();
                });
            },
            many: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/simple/happy/many.xlsx'));
                self.import.process(this.simple.sheet, 'hello', './testdata/xlsx/simple/happy/many.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.simpleTest);
                    test.ok( !! self.import.importData.simpleTest.data);
                    logger.silly(util.inspect(self.import.importData.simpleTest))
                    test.equal(self.import.importData.simpleTest.data.length, 10, "record processing problem.");
                    test.done();
                });
            }
        },
        cellDataProblem: {
            setUp: function(cb) {
                logger.silly('setting up cell data problem');
                var self = this;

                logger.silly('simple');
                self.simple = new simple();
                logger.silly('new ssi');
                self.import = new ss.import();
                logger.silly('cb');
                cb();
            },
            tearDown: function(cb) {
                cb();
            },
            all: function(test) {
                logger.silly('all');
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/simple/cellDataProblem/many.xlsx'));
                self.import.process(self.simple.sheet, 'hello', './testdata/xlsx/simple/cellDataProblem/many.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.simpleTest);
                    test.ok( !! self.import.importData.simpleTest.data);
                    logger.silly(util.inspect(self.import.importData.simpleTest));
                    test.equal(self.import.importData.simpleTest.invalidData.length, 10, "invalid record recognized as valid");
                    test.equal(self.import.importData.simpleTest.data.length, 0, "more than one valid record recognized as invalid");
                    test.done();
                });
            }
        },
        rowDataProblem: {
            setUp: function(cb) {
                this.simple = new simple();
                this.import = new ss.import();
                cb();
            },
            one: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/simple/rowDataProblem/one.xlsx'));
                self.import.process(self.simple.sheet, 'hello', './testdata/xlsx/simple/rowDataProblem/one.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.simpleTest);
                    test.ok( !! self.import.importData.simpleTest.data);
                    logger.silly(util.inspect(self.import.importData.simpleTest));
                    test.equal(self.import.importData.simpleTest.invalidData.length, 1, "invalid record recognized as valid");
                    test.equal(self.import.importData.simpleTest.data.length, 9, "more than one valid record recognized as invalid");
                    test.done();
                });
            },
            all: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/simple/rowDataProblem/many.xlsx'));
                self.import.process(this.simple.sheet, 'hello', './testdata/xlsx/simple/rowDataProblem/many.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.simpleTest);
                    test.ok( !! self.import.importData.simpleTest.data);
                    logger.silly(util.inspect(self.import.importData.simpleTest));
                    test.equal(self.import.importData.simpleTest.invalidData.length, 10, "invalid record recognized as valid");
                    test.equal(self.import.importData.simpleTest.data.length, 0, "more than one valid record recognized as invalid");
                    test.done();
                });
            }
        }
    }
};
