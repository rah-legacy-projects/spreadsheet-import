var ss = require('../index'),
    xlutil = ss.util,
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    util = require('util'),
    simple = require('./testdata/definitions/simpleTest');

logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});

module.exports = exports = {
    setUp: function(cb) {
        cb();
    },
    tearDown: function(cb) {
        cb();
    },
    import: {
        happy: {
            setUp: function(cb) {
                logger.silly('import setup!');
                this.simple = new simple();
                this.import = new ss.import();
                cb();
            },
            one: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/simple/happy/one.xlsx'));
                self.import.process(this.simple.sheet, 'hello', './testdata/xlsx/simple/happy/one.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.simpleTest);
                    test.ok( !! self.import.importData.simpleTest.data);
                    logger.silly(util.inspect(self.import.importData.simpleTest));
                    test.equal(self.import.importData.simpleTest.data.length, 1, "record processing problem.");
                    test.done();
                });
            }
        }
    }
};
