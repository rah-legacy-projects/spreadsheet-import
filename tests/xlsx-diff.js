var ss = require('../index'),
    xlutil = ss.util,
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    util = require('util');

var simple = require('./testdata/definitions/simpleTest');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});
process.on('uncaughtException', function(err) {
    logger.error(err);
    logger.error(err.stack);
});
module.exports = exports = {
    setUp: function(cb) {
        cb();
    },
    tearDown: function(cb) {
        cb();
    },
    import: {
        happy: {
            setUp: function(cb) {

                logger.silly('import setup!');
                this.simple = new simple();
                this.import = new ss.import();
                this.diff = new ss.diff();
                cb();
            },
            many: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/simple/happy/many.xlsx'));
                self.import.process(this.simple.sheet, 'hello', './testdata/xlsx/simple/happy/many.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok(!!result);
                    test.ok(!!self.import.importData);
                    test.ok(!!self.import.importData.simpleTest);
                    test.ok(!!self.import.importData.simpleTest.data);
                    test.equal(self.import.importData.simpleTest.data.length, 10, "record processing problem.");

                    self.diff.process(self.simple, self.import.importData.simpleTest, 'hello', function(err, excelsheet) {
                        test.ifError(err, 'error from diff');
                        test.ok(!!excelsheet);
                        logger.silly(util.inspect(excelsheet, null, 10));
                    });

                    test.done();
                });
            }
        }
    }
};
