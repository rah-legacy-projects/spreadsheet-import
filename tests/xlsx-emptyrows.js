var ss = require('../index'),
    xlutil = ss.util,
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    util = require('util');

var simple = require('./testdata/definitions/simpleTest');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'silly',
    colorize: true
});
process.on('uncaughtException', function(err){
    logger.error(err);
    logger.error(err.stack);
});
module.exports = exports = {
    setUp: function(cb) {
        cb();
    },
    tearDown: function(cb) {
        cb();
    },
    import: {
        'empty rows': {
            setUp: function(cb) {
                this.simple = new simple();
                this.import = new ss.import();
                cb();
            },
            one: function(test) {
                var self = this;
                test.ok(fs.existsSync('./testdata/xlsx/simple/blankRow/one.xlsx'));

                var rowErrors = 0;
                self.import.on('row validation error', function(e) {
                    logger.warn(e.message);
                    rowErrors++;
                });

                self.import.process(this.simple.sheet, 'hello', './testdata/xlsx/simple/blankRow/one.xlsx', function(err, result) {
                    test.ifError(err, 'error from import process');
                    test.ok( !! result);
                    test.ok( !! self.import.importData);
                    test.ok( !! self.import.importData.simpleTest);
                    test.ok( !! self.import.importData.simpleTest.data);
                    test.equal(self.import.importData.simpleTest.invalidData.length, 0, "invalid record recognized as valid");
                    test.equal(self.import.importData.simpleTest.data.length, 9, "more than one valid record recognized as invalid");
                    test.equal(rowErrors, 0, "row errors " + rowErrors);
                    test.done();
                });
            }
        }

    }
};
