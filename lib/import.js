var logger = require('winston'),
    xlsx = require('node-xlsx'),
    fs = require('fs'),
    path = require('path'),
    util = require('util'),
    moment = require('moment'),
    xlutil = require('./xlutil'),
    async = require('async'),
    _ = require('lodash');

var XLSXImport = function() {
        var self = this;
        self.fileName = null;
        self.dataDefinition = null;
        self.token = null;
        self.importData = {};
        self.excelSheets = [];
        self.header = [];

        self.cellProcess = function(defSheet, xlData, xlDataRowIndex, xlColumnIndex, cb) {
            //if the property exists for the given column,
            if (!!self.header[xlColumnIndex]) {
                //add property to "thing"
                //thing[self.header[xlColumnIndex]] = xlData.value;
                //get the def column
                var defColumn = _.find(defSheet.columns, function(c) {
                    return c.name === self.header[xlColumnIndex] || c.propertyName === self.header[xlColumnIndex];
                });
                var value = !!xlData ? xlData.value : '';
                //hack: XLSX parse fails down to numeric checking under certain circumstances and will return 
                //hack: NaN for empty string columns.
                if (/^NaN$/.test(value)) {
                    logger.warn('%s %s %s: NaN is reserved.  Using empty string instead.', defSheet.className, defColumn.name, xlDataRowIndex);
                    value = '';
                }
                //special case: column is a date
                //hack: if the 
                var dateValid = true;
                if (!!defColumn.expectedType && /date/i.test(defColumn.expectedType)) {
                    if (/^(?:(\d{1,2})[\/\-](\d{1,2})[\/\-](\d{4}|\d{2}))?$/.test(value)) {
                        //value is already a date.  moment-validate it to make sure it's a valid date.
                        if (!moment(value)
                            .isValid()) {
                            value = '';
                            dateValid = false;
                        }
                    } else if (/^\-?\d+$/.test(value)) {
                        //value is days since 1900.
                        var d = new Date(1900, 0, 0),
                            off1 = (new Date())
                            .getTimezoneOffset(),
                            off2 = d.getTimezoneOffset();
                        var offset = (off1 - off2) * 60000;
                        var realDate = new Date(+d - offset + (value - 1) * 86400000);
                        //hack: format as mm/dd/yyyy
                        value = (realDate.getMonth() + 1) + "/" + realDate.getDate() + "/" + (1900 + realDate.getYear());
                    } else if (moment(value)
                        .isValid()) {
                        //value was parsed as a real capital-D Date.  Most likely.
                        //hack: format as mm/dd/yyyy
                        value = moment(value)
                            .format("MM/DD/YYYY");
                    } else {
                        value = '';
                        dateValid = false;
                    }
                }

                var validationTasks = [];

                //should there be column validators,
                if (!!defColumn.validators) {
                    var validation = {
                            message: null,
                            sheetName: !!defSheet.sheetName ? defSheet.sheetName : defSheet.className,
                            row: xlDataRowIndex + 1,
                            column: xlColumnIndex + 1,
                            columnName: !!defColumn.name ? defColumn.name : defColumn.propertyName,
                            exception: null
                        }
                        //for each validator,
                    _.each(defColumn.validators, function(validator) {
                        if (_.isFunction(validator.validator)) {
                            var validationTask = function(callback) {
                                validator.validator(value, function(err, result) {
                                    if (!result) {
                                        validation.message = validator.errorMessage;
                                        self.emit('row validation error', validation);

                                        callback(null, {
                                            result: result,
                                            validation: validation
                                        });
                                    } else {
                                        callback(null, {
                                            result: result,
                                            validation: null
                                        });
                                    }

                                });

                            };
                            validationTasks.push(validationTask);

                        } else if (_.isRegExp(validator.validator)) {
                            var validationTask = function(callback) {
                                var result = validator.validator.test(value);
                                if (!result) {
                                    validation.message = validator.errorMessage;
                                    self.emit('row validation error', validation);
                                    callback(null, {
                                        result: result,
                                        validation: validation
                                    });
                                } else {
                                    callback(null, {
                                        result: result,
                                        validation: null
                                    });
                                }

                            };
                            validationTasks.push(validationTask);

                        } else {
                            validation.message = 'Sheet ' + xlworksheet.name + ' has a bad validator on column ' + xlData + ', please contact technical support.';
                            //todo: add bad validators to sheet
                            self.emit('row validation error', validation);
                        }
                    });
                }
                async.series(validationTasks, function(err, results) {
                    //check avalidity
                    var valid = _.reduce(results, function(memo, result) {
                        return memo && !result.validation;
                    }, true);
                    var plucked = _.pluck(results, 'validation');
                    var validations = _.filter(plucked, function(result) {
                        return !!result;
                    });
                    //bubble validity, member name, and member value back up
                    cb(null, {
                        member: self.header[xlColumnIndex],
                        value: value,
                        valid: valid,
                        validations: validations
                    });
                });

            } else {
                //header not found? bubble null so field is ignored.

                cb(null, null);
            }
        };

        self.headerRowProcess = function(defSheet, xlDataRow, xlDataRowIndex, cb) {
            _.each(xlDataRow, function(xlData, xlColumnIndex) {
                //map sheet def columns to xl column ordinals
                var headerColumn = _.find(defSheet.columns, function(c) {
                    return c.name === xlData.value || c.propertyName === xlData.value;
                });
                if (!headerColumn) {
                    //if the sheet has a header that the def does not, mark as error
                    var e = {
                        sheetName: !!defSheet.sheetName ? defSheet.sheetName : defSheet.className,
                        message: 'Sheet ' + (!!defSheet.sheetName ? defSheet.sheetName : defSheet.className) + ' had extra column ' + xlData.value,
                        row: 0,
                        column: xlColumnIndex,
                        columnName: xlData.value,
                        exception: null
                    };
                    self.importData[defSheet.className].errors.push(e);
                    self.emit('sheet warning', e);
                    //add a null header to maintain header array
                    self.header.push(null);
                } else {
                    self.header.push(headerColumn.propertyName);
                }
            });
            //what does the def have that the header doesn't?
            _.each(_.reject(defSheet.columns, function(c) {
                return !!(_.find(xlDataRow, function(d) {
                    return d.value === c.name || d.value === c.propertyName;
                }));
            }), function(h) {
                //if the sheet has a header that the def does not, mark as error
                var e = {
                    sheetName: !!defSheet.sheetName ? defSheet.sheetName : defSheet.className,
                    message: 'Definition ' + (!!defSheet.sheetName ? defSheet.sheetName : defSheet.className) + ' had extra column ' + (!!h.name ? h.name : h.propertyName),
                    row: 0,
                    column: null,
                    columnName: !!h.name ? h.name : h.propertyName,
                    exception: null
                };
                self.importData[defSheet.className].errors.push(e);
                self.emit('sheet warning', e);
            });

            cb(null, null);
        };

        self.dataRowProcess = function(defSheet, xlDataRow, xlDataRowIndex, cb) {
            var valid = true;

            var rowEmptyTasks = [];
            _.each(xlDataRow, function(xlData, xlColumnIndex) {
                var cellEmptyTask = function(cb) {
                    if (xlColumnIndex % 1000 === 0) {
                        setTimeout(function() {
                            var value = !!xlData ? xlData.value : '';
                            value = value.toString() == 'NaN' ? '' : value;
                            cb(null, value == '');
                        }, 0);
                    } else {
                        var value = !!xlData ? xlData.value : '';
                        value = value.toString() == 'NaN' ? '' : value;
                        cb(null, value == '');
                    }
                };
                rowEmptyTasks.push(cellEmptyTask);
            });

            async.parallel(rowEmptyTasks, function(err, r) {
                var rowEmpty = _.reduce(r, function(memo, result) {
                    return memo && result;
                }, true);

                if (rowEmpty) {
                    cb(null, true);
                } else {
                    var cellTasks = [];
                    //for each data item in the row,
                    _.each(xlDataRow, function(xlData, xlColumnIndex) {
                        var cellTask = function(callback) {
                            //hack: prevent the call stack from becoming giiiii-gantic
                            //setTimeout allows node to do some stack cleanup
                            //todo: look into process.nextTick instead
                            //hack: setTimeout for every call causes horrendous lag
                            if (xlColumnIndex % 1000 === 0) {
                                setTimeout(function() {
                                    self.cellProcess(defSheet, xlData, xlDataRowIndex, xlColumnIndex, function(err, result) {
                                        callback(null, result);
                                    });
                                }, 0);
                            } else {
                                self.cellProcess(defSheet, xlData, xlDataRowIndex, xlColumnIndex, function(err, result) {
                                    callback(null, result);
                                });
                            }
                        };
                        cellTasks.push(cellTask);
                    });

                    //hack: right hand items that are empty will not get pulled into the row by nodexlsx.
                    //hack: pad right-hand items in with a for-loop.
                    for (var i = xlDataRow.length; i < self.header.length; i++) {
                        (function(iterator) {
                            cellTasks.push(function(callback) {
                                self.cellProcess(defSheet, null, xlDataRowIndex, iterator, function(err, result) {
                                    callback(err, result);
                                });
                            });
                        })(i);
                    }

                    async.series(cellTasks, function(err, results) {
                        //row validation
                        var valid = _.reduce(results, function(memo, result) {
                            if (result === null) {
                                //hack: treat bad headers as valid so they're ignored
                                return true;
                            }
                            return memo && result.valid;
                        }, true);

                        //assemble the thing
                        var thing = {};
                        thing.errors = [];
                        var nonEmptyMember = false;
                        _.each(results, function(result) {
                            if (!!result) {
                                if (/.+/.test(result.value)) {
                                    nonEmptyMember = true;
                                }
                                thing[result.member] = result.value;
                                _.each(result.validations, function(v) {
                                    thing.errors.push(v);
                                });
                            }
                        });


                        if (!nonEmptyMember) {
                            //if nothing of value was in the line, ignore the row
                            cb(null, null);
                        } else if (valid) {
                            //if there are validators and the row is cell-data valid,
                            if (!!defSheet.rowValidators) {
                                var validation = {
                                    message: null,
                                    sheetName: !!defSheet.sheetName ? defSheet.sheetName : defSheet.className,
                                    row: xlDataRowIndex + 1,
                                    column: null,
                                    columnName: null,
                                    exception: null
                                }
                                var validationTasks = [];

                                //for each row validator,
                                _.each(defSheet.rowValidators, function(validator) {
                                    if (_.isFunction(validator.validator)) {
                                        var validationTask = function(callback) {
                                            validator.validator(thing, function(err, result) {
                                                if (!result) {
                                                    validation.message = validator.errorMessage;
                                                    thing.errors.push(validation);
                                                    self.emit('row validation error', validation);
                                                }
                                                callback(null, result);

                                            });

                                        };
                                        validationTasks.push(validationTask);

                                    } else if (_.isRegExp(validator.validator)) {
                                        var validationTask = function(callback) {
                                            //hack: test the regex against the jsonified thing?
                                            var result = validator.validator.test(JSON.stringify(thing));
                                            if (!result) {
                                                validation.message = validator.errorMessage;
                                                thing.errors.push(validation);
                                                self.emit('row validation error', validation);
                                            }
                                            callback(null, result);
                                        };
                                        validationTasks.push(validationTask);

                                    } else {
                                        validation.message = 'Sheet ' + xlworksheet.name + ' has a bad validator on column ' + xlData.value + ', please contact technical support.';
                                        thing.errors.push(validation);
                                        self.emit('row validation error', validation);
                                    }
                                });

                                async.series(validationTasks, function(err, results) {
                                    var rowvalid = _.reduce(results, function(memo, result) {
                                        return memo && result;
                                    }, true);
                                    if (rowvalid) {
                                        //add the "thing" to the "sheet" in the "return data"
                                        self.importData[defSheet.className].data.push(thing);
                                    } else {
                                        self.importData[defSheet.className].invalidData.push(thing);
                                    }
                                    cb(null, rowvalid);
                                });
                            } else {
                                self.importData[defSheet.className].data.push(thing);
                                cb(null, valid);
                            }
                        } else {

                            self.importData[defSheet.className].invalidData.push(thing);
                            cb(null, valid);
                        }
                    });
                }
            });
        };

        self.rowProcess = function(defSheet, xlDataRow, xlDataRowIndex, cb) {
            var thing = {};
            thing.errors = [];
            //break down the header row
            //header may not be the first row, explicit define or fuzzy header check
            var headerRowIndex = 0;
            if (!!defSheet.headerRow) {
                headerRowIndex = defSheet.headerRow;
            } else if (!!defSheet.fuzzyHeader && self.header.length == 0) {
                //yank column names
                var columnDefNames = _.map(defSheet.columns, function(c) {
                    return c.name || c.propertyName;
                });
                //yank row values
                var rowColumnNames = _.pluck(xlDataRow, 'value');
                //get the intersection count
                var hits = _.intersection(columnDefNames, rowColumnNames)
                    .length;

                if (!!defSheet.fuzzyHeader.threshold) {
                    if (hits > defSheet.fuzzyHeader.threshold) {
                        headerRowIndex = xlDataRowIndex;
                    } else {
                        //ignore the row, we haven't found the header yet
                        cb(null, null);
                        return;
                    }
                } else if (hits > 0) {
                    //close enough
                    headerRowIndex = xlDataRowIndex;
                } else {
                    //ignore the row, we haven't found the header yet
                    cb(null, null);
                    return;
                }

            }

            if (xlDataRowIndex == headerRowIndex) {
                self.headerRowProcess(defSheet, xlDataRow, xlDataRowIndex, function(err, result) {
                    cb(err, result);
                });
            } else {
                self.dataRowProcess(defSheet, xlDataRow, xlDataRowIndex, function(err, result) {
                    cb(err, result);
                });
            }

        };

        self.worksheetProcess = function(xlworksheet, xlworksheetIndex, numberOfWorksheets, cb) {
            //determine sheet from data definition
            var defSheet = null;
            if (_.isArray(self.dataDefinition)) {
                defSheet = _.find(self.dataDefinition, function(s) {
                    return s.sheetName === xlworksheet.name || s.className === xlworksheet.name;
                });
            } else if (_.isObject(self.dataDefinition) && (self.dataDefinition.sheetName === xlworksheet.name || self.dataDefinition.className === xlworksheet.name)) {
                defSheet = self.dataDefinition;
            } else if (!!(self.dataDefinition.sheets)) {
                defSheet = _.find(self.dataDefinition.sheets, function(s) {
                    return s.sheetName === xlworksheet.name || s.className === xlworksheet.name;
                });
            } else {
                //logger.error('worksheet definition was not array, single or in the sheets member');
            }

            if (/^Help -/.test(xlworksheet.name)) {
                logger.info('sheet %s is a helpsheet, ignoring', xlworksheet.name);
                self.emit('sheet info', {
                    sheetName: xlworksheet.name,
                    message: 'Sheet is for help. Ignoring.'
                });
                cb(null, null);
            } else if (/^Errors -/.test(xlworksheet.name)) {
                logger.info('sheet %s is an error sheet, ignoring', xlworksheet.name);
                self.emit('sheet info', {
                    sheetName: xlworksheet.name,
                    message: 'Sheet is for marking errors. Ignoring.'
                });
                cb(null, null);
            } else if (/^Differences -/.test(xlworksheet.name)) {
                logger.info('sheet %s is a diff sheet, ignoring', xlworksheet.name);
                self.emit('sheet info', {
                    sheetName: xlworksheet.name,
                    message: 'Sheet is for marking differences. Ignoring.'
                });
                cb(null, null);
            } else if (!defSheet) {
                logger.warn("sheet for %s not found", xlworksheet.name);
                self.emit('sheet warning', {
                    sheetName: xlworksheet.name,
                    message: 'Sheet not intended for current import.'
                });

                cb(null, null);
            } else if (defSheet.isIgnored) {
                self.emit('sheet info', {
                    sheetName: xlworksheet.name,
                    message: 'Sheet is ignored. Ignoring.'
                });
                logger.info('sheet %s is marked as ignore, ignoring', xlworksheet.name);
                cb(null, null);
            } else {
                self.importData[defSheet.className] = {};
                //data
                self.importData[defSheet.className].data = [];
                //invalid rows (will have row errors)
                self.importData[defSheet.className].invalidData = [];
                //sheet level problems
                self.importData[defSheet.className].errors = [];

                var rowTasks = [];
                var compactedRows = _.filter(_.compact(xlworksheet.data), function(d) {
                    return d.length > 0;
                });
                _.each(compactedRows, function(xlDataRow, xlDataRowIndex) {
                    var rowTask = function(callback) {
                        self.rowProcess(defSheet, xlDataRow, xlDataRowIndex, function(err, result) {
                            self.emit('row progress', {
                                sheetName: xlworksheet.name,
                                current: xlDataRowIndex + 1,
                                total: compactedRows.length
                            });
                            callback(null, result);
                        });
                    };
                    rowTasks.push(rowTask);
                });

                async.series(rowTasks, function(err, results) {
                    //add good data sheet
                    var goodSheet = {};
                    goodSheet.name = !!defSheet.sheetName ? defSheet.sheetName : defSheet.className;
                    goodSheet.data = [];
                    //build the header
                    var xlHeaderRow = [];
                    _.each(defSheet.columns, function(col) {
                        xlHeaderRow.push({
                            value: (!!col.name ? col.name : col.propertyName),
                            formatCode: 'general',
                            bold: 1,
                            backgroundColor: 'FF888888',
                            autoWidth: true,
                            underline: 1
                        });
                    });
                    goodSheet.data.push(xlHeaderRow);
                    _.each(self.importData[defSheet.className].data, function(item, itemIndex) {
                        var xlRow = [];
                        _.each(_.filter(self.header, function(h) {
                            return h !== null;
                        }), function(member) {
                            xlRow.push({
                                value: item[member]
                            });
                        });
                        goodSheet.data.push(xlRow);
                    });
                    self.excelSheets.push(goodSheet);

                    //add bad data sheet with errors
                    var badSheet = {};
                    badSheet.data = [];
                    badSheet.name = "Errors - " + goodSheet.name;

                    //if there are sheet-level errors, build a sheet level header and errors
                    if (!!self.importData[defSheet.className].errors && self.importData[defSheet.className].errors.length > 0) {
                        badSheet.data.push([{
                            value: 'Sheet Errors',
                            formatCode: 'general',
                            bold: 1,
                            backgroundColor: 'FF888888',
                            autoWidth: true,
                            underline: 1
                        }]);
                        _.each(self.importData[defSheet.className].errors, function(error, errorIndex) {
                            badSheet.data.push([{
                                value: error.message
                            }]);
                        });

                        //add a row for effect
                        badSheet.data.push([]);
                    }
                    if (self.importData[defSheet.className].invalidData.length !== 0) {
                        xlHeaderRow.push({
                            value: "Errors",
                            formatCode: 'general',
                            bold: 1,
                            backgroundColor: 'FF888888',
                            autoWidth: true,
                            underline: 1
                        });
                        badSheet.data.push(xlHeaderRow);

                        _.each(self.importData[defSheet.className].invalidData, function(invalidItem, invalidItemIndex) {
                            var xlRow = [];
                            _.each(_.filter(self.header, function(h) {
                                //return h !== null;
                                return true;
                            }), function(member) {
                                if (!member) {
                                    xlRow.push({
                                        value: ''
                                    });
                                } else {
                                    xlRow.push({
                                        value: invalidItem[member]
                                    });
                                }
                            });

                            xlRow.push({
                                value: _.pluck(invalidItem.errors, 'message')
                                    .join(require('os')
                                        .EOL)
                            });

                            badSheet.data.push(xlRow);
                        });
                    }

                    if (badSheet.data.length > 0) {
                        self.excelSheets.push(badSheet);
                    }

                    cb(null, null);
                });
            }
        };

        self.process = function(def, t, fname, cb) {
            self.dataDefinition = def;
            self.token = t;
            self.fileName = fname;
            self.excelSheets = [];

            xlutil.parse(self.fileName, function(err, obj) {

                //make a general import error property
                self.importData.errors = [];

                //emit out how many sheets to expect
                self.emit('sheet progress', {
                    sheetName: null,
                    current: 0,
                    total: obj.worksheets.length
                });
                //for each worksheet from the xls,
                var worksheetTasks = [];
                _.each(obj.worksheets, function(xlworksheet, xlworksheetIndex) {
                    //self.worksheetProcess = function(worksheet, worksheetIndex, cb)
                    var worksheetTask = function(callback) {
                        self.worksheetProcess(xlworksheet, xlworksheetIndex, obj.worksheets.length, function(err, value) {
                            self.emit('sheet progress', {
                                sheetName: xlworksheet.name,
                                current: xlworksheetIndex + 1,
                                total: obj.worksheets.length
                            });
                            callback(null, value);
                        });
                    };
                    worksheetTasks.push(worksheetTask);
                });

                async.series(worksheetTasks, function(err, reults) {
                    self.emit('workbook complete', self.excelSheets);
                    if (!!cb) {
                        cb(null, self.excelSheets);
                    }
                });
            });
        }
        return self;
    }
    //have xlsx import inherit event emitter
XLSXImport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);

//export xlsx import
module.exports = XLSXImport;
