var fs = require('fs'),
    path = require('path'),
    readline = require('readline'),
    stream = require('stream'),
    csv = require('csv'),
    path = require('path'),
    defer = require('node-promise')
        .defer,
    when = require('node-promise')
        .when,
    all = require('node-promise')
        .all,
    util = require('util'),
    zip = require('adm-zip'),
    uuid = require('node-uuid');

var process = function(pathToCSV) {
    var resolvedPath = path.resolve(pathToCSV);
    if (!(/\.csv$/.test(resolvedPath))) {
        throw 'File supplied was not a csv.';
    }

    var sheetName = path.basename(resolvedPath, path.extname(resolvedPath));
    var deferred = defer();
    var data = [];
    csv()
        .from.path(resolvedPath, {
            delimiter: ',',
            escape: '"'
        })
        .on('record', function(row, index) {
            data.push(row);
        })
        .on('end', function(count) {
            deferred.resolve({
                name: sheetName,
                data: data
            });
        })
        .on('error', function(error) {
            cb(error, null);
        });
    return deferred.promise;
};

module.exports = function(pathToFile, cb) {
    var deferreds = [];
    var outputUUID = null;
    var resolvedPathToFile = path.resolve(pathToFile);
    if (/\.csv$/.test(resolvedPathToFile)) {
        deferreds.push(process(resolvedPathToFile));
    } else if (/\.zip$/.test(resolvedPathToFile)) {
        console.log('its a zip!');
        outputUUID = uuid.v4();
        var zipFile = new zip(resolvedPathToFile);
        zipFile.extractAllTo(path.resolve(path.join('.', outputUUID)));
        zipFile.getEntries()
            .forEach(function(zipEntry) {
                console.log('\tentry: ' + zipEntry.entryName);
                deferreds.push(process(path.join('.', outputUUID, zipEntry.entryName)));
            });
    }
    when(all(deferreds), function(responses) {
        console.log(util.inspect(responses));

        //remove uuid directory recursively
        var del = function(path) {
            var files = [];
            if (fs.existsSync(path)) {
                files = fs.readdirSync(path);
                files.forEach(function(file, index) {
                    var f = path + '/' + file;
                    if (fs.lstatSync(f)
                        .isDirectory()) {
                        del(f);
                    } else {
                        fs.unlinkSync(f);
                    }
                });
                fs.rmdirSync(path);
            }
        };
        if ( !! outputUUID) {
            del(path.resolve(path.join('.', outputUUID)));
        }

        cb(null, {worksheets: responses});
    });
};
