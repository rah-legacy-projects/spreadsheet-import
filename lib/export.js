var logger = require('winston'),
    xlsx = require('node-xlsx'),
    fs = require('fs'),
    path = require('path'),
    util = require('util'),
    async = require('async'),
    _ = require('underscore');

var XLSXExport = function() {
    var self = this;
    self.dataDefinition = null;
    self.excelSheets = [];
    self.exportData = {};


    self.memberValidation = function(defSheet, obj, memberName, parameters, dataArrayPos, memberPos, cb) {
        var defColumn = _.filter(defSheet.columns, function(col) {
            return col.propertyName === memberName;
        })[0];
        var validationTasks = [];


        //should there be column validators,
        if ( !! defColumn.validators) {
            var validation = {
                message: null,
                sheetName: !! defSheet.sheetName ? defSheet.sheetName : defSheet.className,
                row: dataArrayPos + 1,
                column: memberPos + 1,
                columnName: !! defColumn.name ? defColumn.name : defColumn.propertyName,
                exception: null
            }
            //for each validator,
            _.each(defColumn.validators, function(validator) {
                if (_.isFunction(validator.validator)) {
                    var validationTask = function(callback) {
                        validator.validator(obj[memberName], function(err, result) {
                            if (!result) {
                                validation.message = validator.errorMessage;
                                self.emit('row validation error', validation);

                                callback(null, {
                                    result: result,
                                    validation: validation
                                });
                            } else {
                                callback(null, {
                                    result: result,
                                    validation: null
                                });
                            }

                        });

                    };
                    validationTasks.push(validationTask);

                } else if (_.isRegExp(validator.validator)) {
                    var validationTask = function(callback) {
                        var result = validator.validator.test(obj[memberName]);
                        if (!result) {
                            validation.message = validator.errorMessage;
                            self.emit('row validation error', validation);
                            callback(null, {
                                result: result,
                                validation: validation
                            });
                        } else {
                            callback(null, {
                                result: result,
                                validation: null
                            });
                        }

                    };
                    validationTasks.push(validationTask);

                } else {
                    validation.message = 'Sheet ' + xlworksheet.name + ' has a bad validator on column ' + xlData + ', please contact technical support.';
                    //todo: add bad validators to sheet
                    self.emit('row validation error', validation);
                }
            });
        }

        async.series(validationTasks, function(err, results) {
            //check validity
            var valid = _.reduce(results, function(memo, result) {
                return memo && !result.validation;
            }, true);
            var plucked = _.pluck(results, 'validation');
            var validations = _.filter(plucked, function(result) {
                return !!result;
            });
            //bubble validity, member name, and member value back up
            cb(null, {
                member: memberName,
                value: obj[memberName],
                valid: valid,
                validations: validations
            });
        });

    };

    self.objectValidation = function(defSheet, obj, parameters, objectPos, cb) {
        var memberTasks = [];
        var memberPos = 0;

        for (var member in obj) {
            //hack: prevent the call stack from becoming giiiii-gantic
            //setTimeout allows node to do some stack cleanup
            //todo: look into process.nextTick instead
            //hack: setTimeout for every call causes horrendous lag
            (function(memberName, i) {
                memberTasks.push(function(callback) {
                    if (i % 100 === 0) {
                        setTimeout(function() {
                            self.memberValidation(defSheet, obj, memberName, parameters, objectPos, memberPos, function(err, valid) {
                                callback(err, valid);
                            })
                        }, 0);

                    } else {
                        self.memberValidation(defSheet, obj, memberName, parameters, objectPos, memberPos, function(err, valid) {
                            callback(err, valid);
                        });
                    }
                });
                memberPos++;
            })(member, memberPos);
        }

        async.series(memberTasks, function(err, r) {
            var valid = _.reduce(r, function(memo, result) {
                return memo && result.valid;
            }, true);

            var thing = _.clone(obj);
            thing.errors = [];
            if (valid) {
                //if there are validators and the row is cell-data valid,
                if ( !! defSheet.rowValidators) {
                    var validation = {
                        message: null,
                        sheetName: !! defSheet.sheetName ? defSheet.sheetName : defSheet.className,
                        row: objectPos + 1,
                        column: null,
                        columnName: null,
                        exception: null
                    }
                    var validationTasks = [];

                    //for each row validator,
                    _.each(defSheet.rowValidators, function(validator) {
                        if (_.isFunction(validator.validator)) {
                            var validationTask = function(callback) {
                                validator.validator(thing, function(err, result) {
                                    if (!result) {
                                        validation.message = validator.errorMessage;
                                        thing.errors.push(validation);
                                        self.emit('row validation error', validation);
                                    }
                                    callback(null, result);

                                });
                            };
                            validationTasks.push(validationTask);

                        } else if (_.isRegExp(validator.validator)) {
                            var validationTask = function(callback) {
                                //hack: test the regex against the jsonified thing?
                                var result = validator.validator.test(JSON.stringify(thing));
                                if (!result) {
                                    validation.message = validator.errorMessage;
                                    thing.errors.push(validation);
                                    self.emit('row validation error', validation);
                                }
                                callback(null, result);
                            };
                            validationTasks.push(validationTask);

                        } else {
                            validation.message = 'Sheet ' + xlworksheet.name + ' has a bad validator on column ' + xlData.value + ', please contact technical support.';
                            thing.errors.push(validation);
                            self.emit('row validation error', validation);
                        }
                    });

                    async.series(validationTasks, function(err, results) {
                        var rowvalid = _.reduce(results, function(memo, result) {
                            return memo && result;
                        }, true);
                        if (rowvalid) {
                            //add the "thing" to the "sheet" in the "return data"
                            self.exportData[defSheet.className].data.push(thing);
                        } else {
                            self.exportData[defSheet.className].invalidData.push(thing);
                        }
                        cb(null, rowvalid);
                    });
                } else {
                    self.exportData[defSheet.className].data.push(thing);
                    cb(null, valid);
                }
            } else {

                self.exportData[defSheet.className].invalidData.push(thing);
                cb(null, valid);
            }

        });

    };

    self.collectionProcess = function(defSheet, collection, exportParameters, cb) {
        var objectTasks = [];
        //if the result was non-null
        if (collection !== null) {
            //if the result of the export data is not an array, bad things
            if (!_.isArray(collection)) {
                self.emit('sheet error', {
                    sheetName: defSheet.sheet.name || defSheet.sheet.className,
                    message: "Export data must be an array."
                });
            } else {
                //otherwise, put together object validation tasks...
                _.each(collection, function(obj, objIndex) {
                    objectTasks.push(function(callback) {
                        self.objectValidation(defSheet.sheet, obj, exportParameters, objIndex, function(err, value) {
                            self.emit('row progress', {
                                sheetName: defSheet.sheet.name || defSheet.sheet.className,
                                current: objIndex + 1,
                                total: collection.length
                            });

                            callback(err, value);
                        });
                    });
                });

                //... and execute them.
                //hack: in series to maintain row progress is in order
                async.series(objectTasks, function(err, r) {
                    //add good data sheet
                    var goodSheet = {};
                    goodSheet.name = !! defSheet.sheet.sheetName ? defSheet.sheet.sheetName : defSheet.sheet.className;
                    goodSheet.data = [];
                    //build the header
                    var xlHeaderRow = [];
                    _.each(defSheet.sheet.columns, function(col) {
                        xlHeaderRow.push({
                            value: ( !! col.name ? col.name : col.propertyName),
                            formatCode: 'general',
                            bold: 1,
                            backgroundColor: 'FF888888',
                            autoWidth: true,
                            underline: 1
                        });
                    });
                    goodSheet.data.push(xlHeaderRow);
                    _.each(self.exportData[defSheet.sheet.className].data, function(item, itemIndex) {
                        var xlRow = [];
                        _.each(_.filter(defSheet.sheet.columns, function(h) {
                            return h !== null;
                        }), function(column) {
                            if ( !! item[column.propertyName]) {
                                xlRow.push({
                                    value: item[column.propertyName]
                                });
                            } else {
                                xlRow.push({
                                    value: ''
                                });
                            }
                        });
                        goodSheet.data.push(xlRow);
                    });
                    self.excelSheets.push(goodSheet);

                    //add bad data sheet with errors
                    var badSheet = {};
                    badSheet.data = [];
                    badSheet.name = "Errors - " + goodSheet.name;

                    //if there are sheet-level errors, build a sheet level header and errors
                    if ( !! self.exportData[defSheet.sheet.className].errors && self.exportData[defSheet.sheet.className].errors.length > 0) {
                        badSheet.data.push([{
                            value: 'Sheet Errors',
                            formatCode: 'general',
                            bold: 1,
                            backgroundColor: 'FF888888',
                            autoWidth: true,
                            underline: 1
                        }]);
                        _.each(self.exportData[defSheet.sheet.className].errors, function(error, errorIndex) {
                            badSheet.data.push([{
                                value: error.message
                            }]);
                        });

                        //add a row for effect
                        badSheet.data.push([]);
                    }
                    if (self.exportData[defSheet.sheet.className].invalidData.length !== 0) {
                        xlHeaderRow.push({
                            value: "Errors",
                            formatCode: 'general',
                            bold: 1,
                            backgroundColor: 'FF888888',
                            autoWidth: true,
                            underline: 1
                        });
                        badSheet.data.push(xlHeaderRow);

                        _.each(self.exportData[defSheet.sheet.className].invalidData, function(invalidItem, invalidItemIndex) {
                            var xlRow = [];
                            _.each(_.filter(defSheet.sheet.columns, function(h) {
                                //return h !== null;
                                return true;
                            }), function(column) {
                                if (!invalidItem[column.propertyName]) {
                                    xlRow.push({
                                        value: ''
                                    });
                                } else {
                                    xlRow.push({
                                        value: invalidItem[column.propertyName]
                                    });
                                }
                            });

                            xlRow.push({
                                value: _.pluck(invalidItem.errors, 'message')
                                    .join(require('os')
                                        .EOL)
                            });

                            badSheet.data.push(xlRow);
                        });
                    }

                    if (badSheet.data.length > 0) {
                        self.excelSheets.push(badSheet);
                    }

                    cb(null);
                });
            }
        }
    };

    self.process = function(def, exportParameters, cb) {
        if ( !! def) {
            self.dataDefinition = def;
        } else {
            logger.warn('using member def');
        }

        //for each worksheet from the export def,
        var worksheets = [];
        if (_.isArray(self.dataDefinition)) {
            _.each(self.dataDefinition, function(x) {
                worksheets.push(x);
            });
        } else if (_.isObject(self.dataDefinition)) {
            worksheets.push(self.dataDefinition);
        } else {
            logger.error('worksheet definition was not array, single or in the sheets member');
        }


        //for each worksheet,
        var tasks = {};
        _.each(worksheets, function(worksheet, worksheetIndex) {
            //pull data from the def's exportData method,
            tasks[worksheet.sheet.className] = function(callback) {
                if (!worksheet.exportData) {
                    logger.warn('no exportData on ' + worksheet.sheet.className);
                    self.emit('sheet error', {
                        sheetName: worksheet.sheet.name || worksheet.sheet.className,
                        message: 'Sheet does not have an exportData method defined.'
                    });
                    callback(null, null);
                } else {
                    worksheet.exportData(exportParameters, function(err, value) {
                        callback(err, value);
                    });
                }
            };
        });

        //execute the data methods in parallel
        async.parallel(tasks, function(err, r) {
            //for each def in the return,


            var collectionTasks = [];
            var worksheetIndex = 0;
            for (var className in r) {
                collectionTasks.push(function(callback) {
                    var defSheet = _.filter(worksheets, function(sheet) {
                        return sheet.sheet.className === className;
                    })[0];
                    self.exportData[defSheet.sheet.className] = {};
                    //data
                    self.exportData[defSheet.sheet.className].data = [];
                    //invalid rows (will have row errors)
                    self.exportData[defSheet.sheet.className].invalidData = [];
                    //sheet level problems
                    self.exportData[defSheet.sheet.className].errors = [];



                    self.collectionProcess(defSheet, r[className], exportParameters, function(err, result) {
                        self.emit('sheet progress', {
                            sheetName: defSheet.sheet.name,
                            current: worksheetIndex + 1,
                            total: worksheets.length
                        });


                        worksheetIndex++;
                        callback(err, result);
                    })
                });
            }

            //emit out how many sheets to expect
            self.emit('sheet progress', {
                sheetName: null,
                current: 0,
                total: collectionTasks.length
            });
            async.series(collectionTasks, function(err, ir) {

                self.emit('workbook complete', self.excelSheets);
                //self.excelSheets = workbook.worksheets;
                if ( !! cb) {
                    cb(null, self.excelSheets);
                }
            });
        });
    }
    return self;
}
//have xlsx export inherit event emitter
XLSXExport.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);

//export xlsx export
module.exports = XLSXExport;
