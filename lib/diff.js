var logger = require('winston'),
    xlsx = require('node-xlsx'),
    fs = require('fs'),
    path = require('path'),
    util = require('util'),
    moment = require('moment'),
    xlutil = require('./xlutil'),
    async = require('async'),
    _ = require('lodash');

var XLSXDiff = function() {
        var self = this;

        self.process = function(type, importedData, t, cb) {
            if (!importedData || !importedData.data) {
                logger.warn('diff data for ' + type.sheet.className + ' on token ' + t + ' does not exist');
                self.emit('diff error', {
                    sheetName: type.sheet.sheetName,
                    message: 'No data found.  Not committing.'
                });
                cb('No data found.  Not committing.', null);
                return;
            }
            self.token = t;

            var idProps = _.map(_.filter(type.sheet.columns, function(c) {
                return c.isIdentity;
            }), function(c) {
                return c.propertyName;
            });

            if (idProps.length == 0) {
                logger.warn('no identity properties found or ' + type.sheet.sheetName);
            }

            if (!type.diff) {
                logger.warn('no diff for ' + type.sheet.className);
                cb('no diff defined');
                return;
            }


            //todo: have EE2 events caught and reemitted here
            //emit out how many rows to expect
            self.emit('diff row progress', {
                sheetName: null,
                current: 0,
                total: importedData.data.length
            });

            var tasks = [];

            _.each(importedData.data, function(row, rowIndex) {
                //for each valid data, call the commit method for the sheet.
                //todo: put commits into an async series array with a single callback to ensure order
                tasks.push(function(callback) {
                    type.diff(row, rowIndex, function(err, loadedRecord) {

                        var differences = [];

                        var newKeys = _.keys(row)
                            .filter(function(k) {
                                return k !== 'errors';
                            });
                        var oldKeys = _.keys(loadedRecord || {});

                        var differenceTemplate = {};
                        _.each(idProps, function(id) {
                            if (!loadedRecord) {
                                differenceTemplate[id] = row[id];
                            } else {
                                differenceTemplate[id] = loadedRecord[id];
                            }
                        });

                        var addDifference = function(changeType, fieldName, oldValue, newValue, message) {
                            var diff = _.clone(differenceTemplate);
                            diff.diffChangeType = changeType;
                            diff.diffFieldName = fieldName;
                            diff.diffOldValue = oldValue;
                            diff.diffNewValue = newValue;
                            diff.diffMessage = message;
                            differences.push(diff);
                        };

                        var getColumn = function(propname) {
                            return _.find(type.sheet.columns, function(column) {
                                return column.propertyName == propname;
                            });
                        };

                        if (!!loadedRecord) {
                            _.each(_.difference(newKeys, oldKeys), function(d) {
                                var col = getColumn(d);
                                addDifference('New Field', col.name || col.propertyName, '', row[d], '');
                            });

                            _.each(_.difference(oldKeys, newKeys), function(d) {
                                var col = getColumn(d);
                                addDifference('Removed Field', col.name || col.propertyName, loadedRecord[d], '', '');
                            });

                            _.each(_.intersection(newKeys, oldKeys), function(d) {
                                var col = getColumn(d);
                                if (/date/i.test(col.expectedType)) {
                                    var oldMoment = moment(loadedRecord[d]);
                                    var newMoment = moment(row[d]);

                                    if((loadedRecord[d] || '') == '' && (row[d] || '') == ''){
                                        //do nothing?
                                    }
                                    else if (!(oldMoment
                                        .isSame(newMoment))) {
                                        addDifference('Modified Record', col.name || col.propertyName, loadedRecord[d], row[d], '');
                                    }
                                } else if (row[d] != loadedRecord[d]) {
                                    addDifference('Modified Record', col.name || col.propertyName, loadedRecord[d], row[d], '');
                                }
                            });
                        } else {
                            _.each(newKeys, function(d) {
                                addDifference('New Record', d, '', row[d], '');
                            });
                        }

                        self.emit('diff row progress', {
                            sheetName: null,
                            current: rowIndex,
                            total: importedData.data.length
                        });



                        callback(null, differences);
                    });
                });
            });

            async.series(tasks, function(err, results) {
                //results are diffs
                //build an excel sheet based on results
                var xlDiffSheet = {};
                xlDiffSheet.name = 'Differences - ' + (type.sheet.sheetName || type.sheet.className);
                xlDiffSheet.data = [];

                if (results.length > 0) {
                    //header
                    var headerRow = [];

                    var keyable = _.find(results, function(result) {
                        return result.length > 0;
                    });

                    if (!!keyable) {
                        keyable = keyable[0];

                        _.each(_.keys(keyable), function(h) {
                            if (h == 'diffChangeType') {
                                headerRow.push({
                                    value: 'Change Type',
                                    formatCode: 'general',
                                    bold: 1,
                                    backgroundColor: 'FF888888',
                                    autoWidth: true,
                                    underline: 1
                                });
                            } else if (h == 'diffFieldName') {

                                headerRow.push({
                                    value: 'Field Name',
                                    formatCode: 'general',
                                    bold: 1,
                                    backgroundColor: 'FF888888',
                                    autoWidth: true,
                                    underline: 1
                                });
                            } else if (h == 'diffOldValue') {
                                headerRow.push({
                                    value: 'Old Value',
                                    formatCode: 'general',
                                    bold: 1,
                                    backgroundColor: 'FF888888',
                                    autoWidth: true,
                                    underline: 1
                                });
                            } else if (h == 'diffNewValue') {
                                headerRow.push({
                                    value: 'New Value',
                                    formatCode: 'general',
                                    bold: 1,
                                    backgroundColor: 'FF888888',
                                    autoWidth: true,
                                    underline: 1
                                });
                            } else if (h == 'diffMessage') {
                                headerRow.push({
                                    value: 'Message',
                                    formatCode: 'general',
                                    bold: 1,
                                    backgroundColor: 'FF888888',
                                    autoWidth: true,
                                    underline: 1
                                });
                            } else {
                                var column = _.find(type.sheet.columns, function(c) {
                                    return c.propertyName == h;
                                });
                                headerRow.push({
                                    value: column.name || column.propertyName,
                                    formatCode: 'general',
                                    bold: 1,
                                    backgroundColor: 'FF888888',
                                    autoWidth: true,
                                    underline: 1
                                });
                            }
                        });

                        xlDiffSheet.data.push(headerRow);

                        _.each(results, function(result) {
                            _.each(result, function(x) {
                                var row = [];
                                _.each(_.keys(x), function(k) {
                                    row.push({
                                        value: x[k]
                                    });
                                });

                                xlDiffSheet.data.push(row);
                            });
                        });
                    } else {
                        headerRow.push({
                            value: "No differences found.",
                            formatCode: 'general',
                            bold: 1,
                            backgroundColor: 'FF888888',
                            autoWidth: true,
                            underline: 1
                        });
                        xlDiffSheet.data.push(headerRow);
                    }
                }

                cb(null, xlDiffSheet);
            });
        }
        return self;
    }
    //have xlsx import inherit event emitter
XLSXDiff.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);

//export xlsx import
module.exports = XLSXDiff;
