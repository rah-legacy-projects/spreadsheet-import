var xlsx = require('node-xlsx'),
    fs = require('fs'),
    path = require('path'),
    logger = require('winston'),
    xlsxParse = require('node-xlsx'),
    csvParse = require('./csv/parse');
module.exports = {
    save: function(worksheets, outputFilePath, cb) {
        var buffer = xlsx.build({
            worksheets: worksheets
        });
        fs.open(outputFilePath, 'w', function(err, fd) {
            if (err) {
                logger.error('error opening file: ' + err);
                return;
            } else {
                fs.write(fd, buffer, 0, buffer.length, null, function(err) {
                    if (err) {
                        logger.error('error writting file: ' + err);
                        return;
                    }
                    fs.close(fd, cb);
                });
            }
        });
    },
    parse: function(filePath, cb) {
        var fullPath = path.resolve(filePath);
        if (/\.csv$/.test(fullPath)) {
            //csv, pass to csv parse
            csvParse(fullPath, cb);
        } else if (/\.zip$/.test(fullPath)) {
            //zip of (hopefully) csvs, pass to csv parse
            csvParse(fullPath, cb);
        } else if (/\.xlsx$/.test(fullPath)) {
            //xlsx, use xlsx parse
            cb(null, xlsxParse.parse(fullPath));
        } else {
            throw 'not supported - trying to parse something for data that is not a csv, xlsx or zip';
        }
    },
    xlDaysToDate: function(value) {
        var d = new Date(1900, 0, 0);
        var off1 = (new Date())
            .getTimezoneOffset(),
            off2 = d.getTimezoneOffset();
        return new Date(+d - offset + (value - 1) * 86400000);
    }
};
