var logger = require('winston'),
    xlsx = require('node-xlsx'),
    fs = require('fs'),
    path = require('path'),
    util = require('util'),
    _ = require('underscore');

var XLSXHelp = function() {
    var self = this;
    self.dataDefinition = null;
    self.excelSheets = {};
    self.process = function(def, cb) {
        if ( !! def) {
            self.dataDefinition = def;
        }
        //for each worksheet from the xls,
        var worksheets = [];
        if (_.isArray(self.dataDefinition)) {
            _.each(self.dataDefinition, function(x) {
                worksheets.push(x);
            });
        } else if (_.isObject(self.dataDefinition)) {
            worksheets.push(self.dataDefinition);
        } else if ( !! (self.dataDefinition.sheets)) {
            _.each(self.dataDefinition.sheets, function(x) {
                worksheets.push(x);
            });
        } else {
            logger.error('worksheet definition was not array, single or in the sheets member');
        }

        var workbook = {
            worksheets: []
        };

        //for each worksheet,
        _.each(worksheets, function(worksheet, worksheetIndex) {
            if ( !! worksheet.skipHelp) {
                logger.info('skipping help for worksheet' + worksheet.className);
            } else {
                var hasHelp = false;

                //create a help sheet
                var name = !! worksheet.sheetName ? worksheet.sheetName : worksheet.className;
                var xlHelpsheet = {};
                xlHelpsheet.data = [];
                xlHelpsheet.name = "Help - " + name;

                //if there is sheet level help,
                if ( !! worksheet.sheetHelp) {
                    hasHelp = true;
                    //build the header row
                    var headerRow = [];
                    headerRow.push({
                        value: 'Sheet Help for ' + name,
                        formatCode: 'general',
                        bold: 1,
                        backgroundColor: 'FF888888',
                        autoWidth: true,
                        underline: 1
                    });
                    xlHelpsheet.data.push(headerRow);

                    //build a help row for each sheet help, ordered by ordinal
                    _.each(_.sortBy(worksheet.sheetHelp, function(h) {
                        if (!h.ordinal) return 0;
                        return h.ordinal;
                    }), function(h2) {
                        var row = [];
                        row.push({
                            value: h2.text
                        });
                        xlHelpsheet.data.push(row);
                    });

                    //todo: merge the first 20ish cells or so for sheet help

                    //add a row for visual effect
                    xlHelpsheet.data.push([]);
                }

                //add a datapoint - help line
                xlHelpsheet.data.push([{
                    value: 'Column',
                    formatCode: 'general',
                    bold: 1,
                    backgroundColor: 'FF888888',
                    autoWidth: true,
                    underline: 1
                }, {
                    value: 'Help',
                    formatCode: 'general',
                    bold: 1,
                    backgroundColor: 'FF888888',
                    autoWidth: true,
                    underline: 1
                }]);

                //for each ordinally sorted column, build column name - help by-newline-pairs
                _.each(worksheet.columns, function(column, columnIndex) {
                    if ( !! column.columnHelp) {
                        hasHelp = true;
                        var helpText = _.pluck(_.sortBy(column.columnHelp, function(ch) {
                            if (!ch.ordinal) return 0;
                            return ch.ordinal;
                        }), 'text')
                            .join(require('os')
                                .EOL);

                        xlHelpsheet.data.push([{
                            value: !! column.name ? column.name : column.propertyName,
                            bold: 1
                        }, {
                            value: helpText
                        }]);
                    }
                });
                //add the help sheet to the workbook
                if (hasHelp) {
                    workbook.worksheets.push(xlHelpsheet);
                } else {
                    logger.info('sheet ' + name + ' does not have any help.  SKipping.');
                }
            }
            self.emit('sheet progress', {
                sheetName: worksheet.name,
                current: worksheetIndex + 1,
                total: worksheets.length
            });

        });
        self.emit('workbook complete', workbook.worksheets);
        self.excelSheets = workbook.worksheets;
        if ( !! cb) {
            cb(null, self.excelSheets);
        }
    }
    return self;
}
//have xlsx import inherit event emitter
XLSXHelp.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);

//export xlsx import
module.exports = XLSXHelp;
