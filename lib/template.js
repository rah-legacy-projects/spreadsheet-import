var logger = require('winston'),
    xlsx = require('node-xlsx'),
    fs = require('fs'),
    path = require('path'),
    util = require('util'),
    _ = require('underscore');

var XLSXTemplate = function() {
    var self = this;
    self.dataDefinition = null;
    self.excelSheets = {};
    self.process = function(def, cb) {
        if(!!def){
            self.dataDefinition = def;
        }

        //for each worksheet from the xls,
        var worksheets = [];
        if (_.isArray(self.dataDefinition)) {
            _.each(self.dataDefinition, function(x) {
                worksheets.push(x);
            });
        } else if (_.isObject(self.dataDefinition)) {
            worksheets.push(self.dataDefinition);
        } else if ( !! (self.dataDefinition.sheets)) {
            _.each(self.dataDefinition.sheets, function(x) {
                worksheets.push(x);
            });
        } else {
            logger.error('worksheet definition was not array, single or in the sheets member');
        }


        var workbook = {
            worksheets: []
        };
        //for each worksheet,
        _.each(worksheets, function(worksheet, worksheetIndex) {
            var xlWorksheet = {};
            xlWorksheet.name = ( !! worksheet.sheetName ? worksheet.sheetName : worksheet.className);
            xlWorksheet.data = [];
            //build a header row,
            var headerRow = [];
            _.each(worksheet.columns, function(col) {
                headerRow.push({
                    value: ( !! col.name ? col.name : col.propertyName),
                    formatCode: 'general',
                    bold: 1,
                    backgroundColor: 'FF888888',
                    autoWidth: true,
                    underline: 1
                });
            });
            //push the header into the data set
            xlWorksheet.data.push(headerRow);

            //push the sheet into the workbook
            workbook.worksheets.push(xlWorksheet);

            self.emit('sheet progress', {
                sheetName: worksheet.name,
                current: worksheetIndex + 1,
                total: worksheets.length
            });

        });


        

        self.emit('workbook complete', workbook.worksheets);
        self.excelSheets = workbook.worksheets;
        if(!!cb){cb(null, self.excelSheets);}
    }
    return self;
}
//have xlsx import inherit event emitter
XLSXTemplate.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);

//export xlsx import
module.exports = XLSXTemplate;
