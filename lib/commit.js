var logger = require('winston'),
    xlsx = require('node-xlsx'),
    fs = require('fs'),
    path = require('path'),
    util = require('util'),
    _ = require('underscore'),
    async = require('async');

var XLSXCommit = function() {
    var self = this;
    self.token = null;

    self.process = function(type, importedData, t, cb) {
        if(!importedData || !importedData.data){
            logger.warn('commit data for ' + type + ' on token ' + t + ' does not exist');
            self.emit('commit error', {
                sheetName: type.sheet.sheetName,
                message: 'No data found.  Not committing.'
            });
            cb('No data found.  Not committing.',null);
            return;
        }
        self.token = t;

        var sheetType = type;

        //todo: have EE2 events caught and reemitted here
        //emit out how many rows to expect
        self.emit('commit row progress', {
            sheetName: null,
            current: 0,
            total: importedData.data.length
        });

        var tasks = [];
        _.each(importedData.data, function(row, rowIndex) {
            //for each valid data, call the commit method for the sheet.
            //todo: put commits into an async series array with a single callback to ensure order
            tasks.push(function(callback) {
                sheetType.commit(row, rowIndex, function(err) {
                    if ( !! err) {
                        //todo: move this into sheet?
                        //todo: better error message?
                        //todo: sheet commit cb "err" has message member?
                        self.emit('commit row error', {
                            sheetName: !! sheetType.sheetName ? sheetType.sheetName : sheetType.className,
                            current: rowIndex + 1,
                            message: err,
                            exception: err
                        });
                    } else {
                        self.emit('commit row progress', {
                            sheetName: !! sheetType.sheetName ? sheetType.sheetName : sheetType.className,
                            current: rowIndex + 1,
                            total: importedData.data.length
                        });
                    }
                    callback(err, null);
                });
            });
        });
        //self.emit('commit sheet progress', {
        //    sheetName: !! sheetType.sheetName ? sheetType.sheetName : sheetType.className,
        //    current: sheetMemberIndex + 1,
        //    total: Object.keys(_excelProcessData.importData)
        //        .length
        //});

        async.series(tasks, function(err, results) {
            if(!!err){
                logger.error(err);
            }
            if ( !! cb) {
                cb(null, 'todo');
            }
        });
    }
    return self;
}
//have xlsx import inherit event emitter
XLSXCommit.prototype = Object.create(require('eventemitter2')
    .EventEmitter2.prototype);

//export xlsx import
module.exports = XLSXCommit;
